var app = angular.module('litsafeApp', ['smoothScroll', 'ui-notification', 'ngAnimate']);

app.controller('navController', function($scope) {

    $scope.w3_open = function() {
        document.getElementById('sidenav').getElementsByClassName('side-content')[0].className += " open";
        document.getElementById('myOverlay').style.display = "block";
        document.getElementById('closebtn').style.display = "block";
    };

    $scope.w3_close = function() {
        document.getElementById('sidenav').getElementsByClassName('side-content')[0].className = "side-content";
        document.getElementById('myOverlay').style.display = "none";
        document.getElementById('closebtn').style.display = "none";
    };
});

app.controller('formController', function ($scope, Notification, $timeout) {
    $scope.searchButtonText = "SEND";

    $scope.submitForm = function(user) {
        $scope.searchButtonText = "SENDING";

        if ($scope.userForm.$valid) {
            var service_id = 'gmail';
            var template_id = 'litsafe_templates';
            var template_params = {
                email: user.email,
            };

            emailjs.send(service_id,template_id,template_params)
            .then(function(response) {
              Notification.success({message: 'Your email was sent successfully. Thanks.', positionY: 'top', positionX: 'right'});
                $timeout(function(){
                   window.location.reload();
                }, 5000);
            }, function(err) {
              Notification.error({message: 'Failed to send your email. Please try again.', positionY: 'top', positionX: 'right'});
              $scope.searchButtonText = "SEND";
            });
        }
    };
});